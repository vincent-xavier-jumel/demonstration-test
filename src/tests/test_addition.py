import pytest
from addition.addition import *

def test_addition():
    assert addition(2, 5) == 7

def test_addition_non_int():
    with pytest.raises(Exception):
        addition(2, 1.5)
    