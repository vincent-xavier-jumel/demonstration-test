def addition(a: int, b: int) -> int:
    assert isinstance(a, int) and isinstance(b, int)
    return a + b